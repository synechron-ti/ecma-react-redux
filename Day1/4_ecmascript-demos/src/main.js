// Import an entire module, for side effects only, without importing anything from the file.
// This will run the module's global code, but doesn't import any values.

// import './1_datatypes/1_declarations';
// import './1_datatypes/2_es6-declarations';
// import './1_datatypes/3_es6-const';
// import './1_datatypes/4_datatypes';
// import './1_datatypes/5_operators';
// import './1_datatypes/6_symbols';

// import './2_functions/1_fn-creation';
// import './2_functions/2_fn-parameters';
// import './2_functions/3_rest-and-spread';
// import './2_functions/4_pure-impure-fn';
// import './2_functions/5_fn-overloading';
// import './2_functions/6_fn-as-arguments';
// import './2_functions/7_using-callbacks';
// import './2_functions/8_fn-context';
// import './2_functions/9_closure';
// import './2_functions/10_fn-currying';
// import './2_functions/11_hof';

import './3_collection/1_arrays';

// console.log("Main, a is:", a);