'use strict'

// let count = 0;

// function next() {
//     return count += 1;
// }

// setInterval(() => {
//     console.log(next());
// }, 2000);

// setTimeout(() => {
//     count = "abc";
// }, 5000);

// // ------------------------------------------

// function next() {
//     let count = 0;
//     return count += 1;
// }

// setInterval(() => {
//     console.log(next());
// }, 2000);

// // setTimeout(() => {
// //     count = "abc";
// // }, 5000);

// ------------------------------------------

// function getNext() {
//     let count = 0;

//     return function() {
//         return count += 1;
//     }
// }

// const next = getNext();

// const next = (function () {
//     let count = 0;

//     return function () {
//         return count += 1;
//     }
// })();

// setInterval(() => {
//     console.log(next());
// }, 2000);

const counter = (function () {
    let count = 0;

    return {
        next: function () {
            return count += 1;
        },
        prev: function () {
            return count -= 1;
        },
        reset: function () {
            count = 0;
        }
    };
})();

setInterval(() => {
    console.log(counter.next());
}, 2000);

setInterval(() => {
    console.log(counter.prev());
}, 5000);
