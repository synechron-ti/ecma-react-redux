'use strict'

// // Dev 1
// function getString() {
//     const strArr = ["NodeJS", "ReactJS", "Angular", "ExtJS", "jQuery"];
//     let str = strArr[Math.floor(Math.random() * strArr.length)];
//     return str;
// }


// // Dev 2
// // let s = getString();
// // console.log(s);

// setInterval(() => {
//     let s = getString();
//     console.log(s);
// }, 2000);

// ----------------------------------------
// If the method is Async or the time taken to execute the logic is inderterminate then we can use Callbacks

// Dev 1
function pushString(callback) {
    const strArr = ["NodeJS", "ReactJS", "Angular", "ExtJS", "jQuery"];

    function scheduleNext() {
        let str = strArr[Math.floor(Math.random() * strArr.length)];
        let randomInterval = Math.floor(Math.random() * 5000) + 1000; // Random interval between 1s and 5s
        setTimeout(scheduleNext, randomInterval);
        callback(str, randomInterval);
    }

    scheduleNext();
}

// Dev 2
pushString((str, interval) => {
    console.log(`${str} received, next string in ${interval} ms`);
});