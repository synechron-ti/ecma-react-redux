// var employees = new Array();
// var employees = new Array(2);

// employees.push("Manish");
// employees.unshift("Abhijeet");

// employees[1] = "Ramesh";
// employees.splice(2, 1, "Ramakant");

// var employees = new Array("Manish", "Ramesh", "Suresh");
// var employees = ["Manish", "Ramesh", "Suresh"];

// var employees = new Array("Manish");
// var employees = new Array(10);
// var employees = Array.of(10);

// var arr = ["Manish", "Ramesh", "Suresh"];
// // var employees = new Array(arr);
// // var employees = Array.from(arr);
// var employees = [...arr];

// console.log(employees);
// console.log(typeof employees);
// console.log(employees instanceof Array);
// console.log(employees.length);

// -------------------------------------- Iterate

var employees = [
    { id: 1, name: "Manish" },
    { id: 2, name: "Varun" },
    { id: 3, name: "Paresh" },
    { id: 4, name: "Devesh" },
    { id: 5, name: "Atul" },
    { id: 6, name: "Abhishek" }
];

delete employees[2];

// for (let i = 0; i < employees.length; i++) {
//     console.log(`${i}        ${JSON.stringify(employees[i])}`);
// }

// for (let i in employees) {
//     console.log(`${i}        ${JSON.stringify(employees[i])}`);
// }

// employees.forEach((item, index) => {
//     console.log(`${index}        ${JSON.stringify(item)}`);
// });

// ECMASCRIPT 2015 - for-of loop
// for (let item of employees) {
//     console.log(JSON.stringify(item));
// }

// for (let pair of employees.entries()) {
//     console.log(JSON.stringify(pair));
// }

for (const [index, item] of employees.entries()) {
    console.log(`${index}        ${JSON.stringify(item)}`);
}