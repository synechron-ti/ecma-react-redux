'use strict'

// Create a function, which should compare and give true, only if, color const is passed to it as an argument

// // ------------------------------------------------------------------- Primitive Type (Immutable)
// const color = "red";

// function isRedColor(arg) {
//     console.log(arg === color);
// }

// isRedColor(color);

// isRedColor("red");

// let clr = "red";
// isRedColor(clr);

// // ------------------------------------------------------------------- Object Type (Mutable)
// const color = { code: "red" };

// function isRedColor(arg) {
//     console.log(arg === color);
// }

// isRedColor(color);

// isRedColor({ code: "red" });

// let clr = { code: "red" };
// isRedColor(clr);


// ------------------------------------------------------------------- Primitive Type (Immutable) - Symbol
// Unique Immutable Type (Symbol)

const color = Symbol("red");

function isRedColor(arg) {
    console.log(arg === color);
}

isRedColor(color);

isRedColor(Symbol("red"));

let clr = Symbol("red");
isRedColor(clr);