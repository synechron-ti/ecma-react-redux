'use strict'
// let a;
// console.log("a is:", a);

// // const env;              // SyntaxError: Missing initializer in const declaration
// // console.log("env is:", env);

// const env = "dev";
// console.log("env is:", env);

// a = 20;
// console.log("a is:", a);

// env = "prod";           // TypeError: Assignment to constant variable.
// console.log("env is:", env);

// --------------------------------------------- Redeclaration
// You cannot create a variable with same name using const keyword

// const a = 10;
// const a = "Hello";
// console.log("a is:", a);
// console.log("Type of a is:", typeof a);

// ----------------------------------------------------
// Const supports Block Scoping

// const env = "dev";
// console.log("Outside Block, env is:", env);

// if (true) {
//     const env = "prod";
//     console.log("Inside Block, env is:", env);
// }

// --------------------------------

const person = { id: 1, name: "John" };
console.log("person is:", person);  

person.id = 1000;
person.name = "Doe";
console.log("person is:", person);

person.city = "Bangalore";
console.log("person is:", person);

delete person.name;
console.log("person is:", person);

person = { id: 2, name: "Jane" };        // TypeError: Assignment to constant variable.
console.log("person is:", person);