'use strict'

// When a language is not typesafe, what does it implies in terms of writing code?
// We cannot specify the type while declaring a variable
// We can provide any value to any variable
// We can use any operator with any operand

// let result = 10 + "20";
// console.log("result is:", result);

// JavaScript automatically converts the boolean value true to a number in this context.
// let result = 10 * true;
// console.log("result is:", result);

// let result = 10 * undefined;
// console.log("result is:", result);

// console.log(true && "abc");
// console.log(false && "abc");

// console.log(true ? "abc" : "xyz");
// console.log(false ? "abc" : "xyz");

// console.log(true && "abc" || "xyz");
// console.log(false && "abc" || "xyz");

// let obj;
// // let obj = undefined;
// // let obj = null;
// // let obj = { id: 1 };

// // if ((obj === null) || (obj === undefined)) {
// //     console.error("obj is null or undefined");
// // } else {
// //     console.log("obj is not null or undefined, value is:", obj);
// // }

// if (!obj) {
//     console.error("obj is null or undefined");
// } else {
//     console.log("obj is not null or undefined, value is:", obj);
// }

// ---------------------------------------------- Compare

// let a = 10;
// let b = "10";

// // console.log(typeof a);
// // console.log(typeof b);

// console.log(a == b);            // Abstract Equality Comparison
// console.log(a === b);           // Strict Equality Comparison

let a = { id: 0 };
let b = { id: 0 };

console.log(a == b);            // Abstract Equality Comparison
console.log(a === b);           // Strict Equality Comparison

// Reference Copy
let c = b;
console.log(b == c);            // Abstract Equality Comparison
console.log(b === c);           // Strict Equality Comparison