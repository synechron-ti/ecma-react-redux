// import authenticatorClient from "./authenticator-api-client";

const url = process.env.REACT_APP_PRODUCTS_API_URL;

// const productAPIClient = {
//     getAllProducts: function () {
//         return new Promise((resolve, reject) => {
//             let fData = {
//                 method: "GET",
//                 headers: {
//                     "accept": "application/json",
//                     // "x-access-token": authenticatorClient.getToken()
//                 }
//             };

//             fetch(url, fData).then((response) => {
//                 response.json().then((data) => {
//                     if (response.status === 403)
//                         reject(data.message);
//                     else
//                         resolve(data);
//                 }).catch((err) => {
//                     reject("Parsing Error");
//                 })
//             }).catch((err) => {
//                 reject("Communication Error");
//             });
//         });
//     }
// }

const productAPIClient = {
    getAllProducts: function () {
        var promise = new Promise((resolve, reject) => {
            return fetch(url).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                });
            }).catch((err) => {
                console.log(err);
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    insertProduct: function (p) {
        const request = new Request(url, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(p)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    updateProduct: function (p) {
        const request = new Request(url + "/" + p.id, {
            method: 'PUT',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(p)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    deleteProduct: function (p) {
        const request = new Request(url + "/" + p.id, {
            method: 'DELETE'
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then(() => {
                    resolve("Record Deleted");
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    }
}

export default productAPIClient;