const url = process.env.REACT_APP_AUTHENTICATOR_API_URL;

const authenticatorClient = {
    isAuthenticated: false,

    logIn: function (uname, pwd) {
        return new Promise((resolve, reject) => {
            var data = `username=${uname}&password=${pwd}`;

            let fData = {
                method: "POST",
                headers: {
                    "content-type": "application/x-www-form-urlencoded"
                },
                body: data
            };

            fetch(url, fData).then((response) => {
                response.json().then((data) => {
                    if (data.success) {
                        localStorage.setItem("s_access_token", data.token);
                        this.isAuthenticated = true;
                        resolve("Login Success");
                    }
                    else {
                        reject(data.message);
                    }
                }).catch((err) => {
                    reject("Parsing Error");
                })
            }).catch((err) => {
                reject("Communication Error");
            });
        });
    },

    logOut: function () {
        localStorage.removeItem("s_access_token");
        this.isAuthenticated = false;
    },

    getToken: function () {
        return localStorage.getItem("s_access_token");
    },
}

export default authenticatorClient;