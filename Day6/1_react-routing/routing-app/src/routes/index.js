import { lazy, Suspense } from 'react';
import { Routes, Route, Navigate, useLocation } from "react-router-dom";

import authenticatorClient from "../services/authenticator-api-client";

// Eager Loading
import LoaderAnimation from '../components/common/LoaderAnimation';
// import HomeComponent from "../components/home/HomeComponent";
// import AboutComponent from "../components/about/AboutComponent";
// import NoMatchComponent from "../components/no-match/NoMatchComponent";
// import ProductsComponent from "../components/products/ProductsComponent";
// import AdminComponent from "../components/admin/AdminComponent";
// import LoginComponent from "../components/login/LoginComponent";
// import ProductsProvider from "../context/ProductsContext";
// import ProductNotSelectedComponent from "../components/products/ProductsNotSelectedComponent";
// import ProductDetailsComponent from "../components/products/ProductDetailsComponent";
// import ReducerHookDemo from "../components/reducer-hook/ReducerHookDemo";

// Lazy Loading
const HomeComponent = lazy(() => import("../components/home/HomeComponent"));
const AboutComponent = lazy(() => import("../components/about/AboutComponent"));
const NoMatchComponent = lazy(() => import("../components/no-match/NoMatchComponent"));
const ProductsComponent = lazy(() => import("../components/products/ProductsComponent"));
const AdminComponent = lazy(() => import("../components/admin/AdminComponent"));
const LoginComponent = lazy(() => import("../components/login/LoginComponent"));
const ProductsProvider = lazy(() => import("../context/ProductsContext"));
const ProductNotSelectedComponent = lazy(() => import("../components/products/ProductsNotSelectedComponent"));
const ProductDetailsComponent = lazy(() => import("../components/products/ProductDetailsComponent"));
const ReducerHookDemo = lazy(() => import("../components/reducer-hook/ReducerHookDemo"));

const SecuredRoute = ({ children }) => {
    let location = useLocation();

    if (authenticatorClient.isAuthenticated) {
        return children;
    } else {
        return <Navigate to="/login" state={{ from: location }} />;
    }
}

export default (
    <Suspense fallback={<LoaderAnimation />}>
        <Routes>
            <Route path="/" element={<HomeComponent />} />
            <Route path="/about" element={<AboutComponent />} />
            <Route path="/products" element={<ProductsProvider><ProductsComponent /></ProductsProvider>}>
                <Route path="" element={<ProductNotSelectedComponent />} />
                <Route path=":productId" element={<ProductDetailsComponent />} />
            </Route>
            <Route path="/admin" element={
                <SecuredRoute>
                    <AdminComponent />
                </SecuredRoute>
            } />
            <Route path="/reducer" element={<ReducerHookDemo />} />
            <Route path="/login" element={<LoginComponent />} />
            <Route path="*" element={<NoMatchComponent />} />
        </Routes>
    </Suspense>
);