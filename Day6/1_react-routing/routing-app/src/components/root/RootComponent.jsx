import React, { useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import NavigationComponent from '../bs-nav/NavigationComponent';
import fetchIntercept from 'fetch-intercept';
import authenticatorClient from '../../services/authenticator-api-client';

const RootComponent = () => {
  useEffect(() => {
    const unregister = fetchIntercept.register({
      request: function (url, config) {
        config = config || {};
        config.headers = config.headers || {};

        // Add the default header
        config.headers['Accept'] = 'application/json';
        
        // Modify the url or config here
        if (url.includes('products')) {
          config.headers['x-access-token'] = authenticatorClient.getToken();
        }
        return [url, config];
      },

      requestError: function (error) {
        // Called when an error occured during another 'request' interceptor call
        return Promise.reject(error);
      },

      response: function (response) {
        // Modify the response object
        return response;
      },

      responseError: function (error) {
        // Handle an fetch error
        return Promise.reject(error);
      }
    });

    return () => {
      unregister();
    };
  }, []);

  return (
    <div className='container'>
      <Router>
        <NavigationComponent />
      </Router>
    </div>
  );
};

export default RootComponent;