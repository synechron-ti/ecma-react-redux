import React, { useReducer } from 'react';

const counterState = { count: 0 };

const counterReducer = (state, action) => {
    switch (action.type) {
        case counterActionTypes.increment:
            return { count: state.count + action.payload };
        case counterActionTypes.decrement:
            return { count: state.count - action.payload };
        default:
            return state;
    }
}

const counterActionTypes = {
    increment: 'increment',
    decrement: 'decrement'
};

const Counter = function ({ interval = 1 }) {
    const [state, dispatch] = useReducer(counterReducer, counterState);

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={state.count} readOnly />
                <button className="btn btn-info" onClick={() => { dispatch({ type: counterActionTypes.increment, payload: interval }); }}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info" onClick={() => { dispatch({ type: counterActionTypes.decrement, payload: interval }); }}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

const CounterSibling = function ({ interval = 1 }) {
    const [state, dispatch] = useReducer(counterReducer, counterState);

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Sibling Component</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={state.count} readOnly />
                <button className="btn btn-info" onClick={() => { dispatch({ type: counterActionTypes.increment, payload: interval }); }}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info" onClick={() => { dispatch({ type: counterActionTypes.decrement, payload: interval }); }}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

const ReducerHookDemo = () => {
    return (
        <div>
            <Counter interval={10} />
            <hr />
            <CounterSibling />
        </div>
    );
};

export default ReducerHookDemo;