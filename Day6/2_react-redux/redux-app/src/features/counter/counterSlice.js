import { createSlice } from "@reduxjs/toolkit";

const counterState = { count: 0 };

export const counterSlice = createSlice({
    name: 'counter',
    initialState: counterState,
    reducers: {
        increment: (state) => {
            state.count += 1;
        },
        decrement: (state) => {
            state.count -= 1;
        },
        incrementBy: (state, action) => {
            state.count += action.payload;
        },
        decrementBy: (state, action) => {
            state.count -= action.payload;
        },
    }
});

export const incrementAsync = function(amount) {
    return function(dispatch) {
        setTimeout(() => {
            dispatch(incrementBy(amount));
        }, 1000);
    }
}

export const decrementAsync = function(amount) {
    return function(dispatch) {
        setTimeout(() => {
            dispatch(decrementBy(amount));
        }, 1000);
    }
}

export const { increment, decrement, incrementBy, decrementBy } = counterSlice.actions;

export default counterSlice.reducer;