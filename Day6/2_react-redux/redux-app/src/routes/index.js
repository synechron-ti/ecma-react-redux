import { lazy, Suspense } from "react";
import { Routes, Route } from "react-router-dom";

import LoaderAnimation from "../components/common/LoaderAnimation";
const HomeComponent = lazy(() => import("../components/home/HomeComponent"));
const AboutComponent = lazy(() => import("../components/about/AboutComponent"));
const NoMatchComponent = lazy(() => import("../components/no-match/NoMatchComponent"));
const CounterRoot = lazy(() => import("../components/using-context/CounterRoot"));
const CounterMain = lazy(() => import("../components/counter/CounterMain"));
const ProductsComponent = lazy(() => import("../components/products/ProductsComponent"));
const ManageProductComponent = lazy(() => import("../components/products/ManageProductComponent"));

export default (
    <Suspense fallback={<LoaderAnimation />}>
        <Routes>
            <Route path="/" element={<HomeComponent />} />
            <Route path="/about" element={<AboutComponent />} />
            <Route path="/counter-context" element={<CounterRoot />} />
            <Route path="/counter" element={<CounterMain />} />
            <Route path="/products" element={<ProductsComponent />} />
            <Route path="/product/:id" element={<ManageProductComponent />} />
            <Route path="/product" element={<ManageProductComponent />} />
            <Route path="*" element={<NoMatchComponent />} />
        </Routes>
    </Suspense>
);