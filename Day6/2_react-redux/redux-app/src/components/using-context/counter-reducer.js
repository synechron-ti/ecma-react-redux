import { counterActions } from './counter-actions';

export const counterState = { count: 0 };

export const counterReducer = (state, action) => {
    switch (action.type) {
        case counterActions.increment:
            return { count: state.count + action.payload };
        case counterActions.decrement:
            return { count: state.count - action.payload };
        default:
            return state;
    }
}