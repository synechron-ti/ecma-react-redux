import { createContext, useContext, useReducer } from "react";
import { counterReducer, counterState } from "./counter-reducer";

const CounterContext = createContext();

export function useCounter() {
    const context = useContext(CounterContext);

    if (!context) {
        throw new Error('useCounter must be used within a CounterProvider');
    }

    return context;
}

export const CounterProvider = ({ children }) => {
    const [state, dispatch] = useReducer(counterReducer, counterState);

    const providerValue = { counterState: state, counterDispatch: dispatch };

    return (
        <CounterContext.Provider value={providerValue}>
            {children}
        </CounterContext.Provider>
    );
};