export const counterActions = {
    increment: 'increment',
    decrement: 'decrement',
}

export function incrementAction(payload) {
    return { type: counterActions.increment, payload };
}

export function decrementAction(payload) {
    return { type: counterActions.increment, payload };
}