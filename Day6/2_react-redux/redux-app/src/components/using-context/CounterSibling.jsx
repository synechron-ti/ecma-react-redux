import { decrementAction, incrementAction } from "./counter-actions";
import { useCounter } from "./counter-context";

const CounterSibling = function ({ interval = 1 }) {
    const context = useCounter();

    return (
        <>
            <div className="text-center">
                <h3 className="text-info"> Sibling Component</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={context.counterState.count} readOnly />
                <button className="btn btn-info" onClick={() => { context.counterDispatch(incrementAction(interval)); }}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info" onClick={() => { context.counterDispatch(decrementAction(interval)); }}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

export default CounterSibling;