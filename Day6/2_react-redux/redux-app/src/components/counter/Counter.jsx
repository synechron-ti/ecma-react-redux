import { useDispatch, useSelector } from 'react-redux';
import { decrement, decrementAsync, increment, incrementAsync } from '../../features/counter/counterSlice';
import styles from './Counter.module.css';

const Counter = function ({ interval = 1 }) {
    const count = useSelector((state) => state.counter.count);
    const dispatch = useDispatch();

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component Using Redux</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={count} readOnly />
                <button className="btn btn-info" onClick={() => { dispatch(increment()); }}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info" onClick={() => { dispatch(decrement()); }}>
                    <span className='fs-4'>-</span>
                </button>

                <button className={`${styles.asyncButton} btn btn-warning`} onClick={() => { dispatch(incrementAsync(5)); }}>
                    <span className='fs-4'>+ Async</span>
                </button>
                <button className={`${styles.asyncButton} btn btn-warning`} onClick={() => { dispatch(decrementAsync(5)); }}>
                    <span className='fs-4'>- Async</span>
                </button>
            </div>
        </>
    );
}

export default Counter;