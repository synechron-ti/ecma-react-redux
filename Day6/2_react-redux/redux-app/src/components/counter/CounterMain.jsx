import React from 'react';
import Counter from './Counter';
import CounterSibling from './CounterSibling';

const CounterMain = () => {
    return (
        <div>
            <Counter />
            <hr />
            <CounterSibling interval={10} />
        </div>
    );
};

export default CounterMain;