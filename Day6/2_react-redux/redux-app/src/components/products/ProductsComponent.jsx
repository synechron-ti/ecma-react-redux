import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ConfirmModal from '../common/ConfirmModal';

import ProductListComponent from './ProductListComponent';
import LoaderAnimation from '../common/LoaderAnimation';

import { useEffect, useState } from 'react';
import { deleteProduct, fetchProducts } from '../../features/products/productsSlice';
import AddProductButton from './AddProductButton';

const ProductsComponent = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.items);
    const status = useSelector(state => state.products.status);
    const error = useSelector(state => state.products.error);

    const [show, setShow] = useState(false);
    const [productToDelete, setproductToDelete] = useState(false);

    useEffect(() => {
        if (status === 'idle')
            dispatch(fetchProducts());
    }, [dispatch, status]);

    const handleRefresh = () => {
        dispatch(fetchProducts());
    }

    const handleDeleteProduct = (product) => {
        setShow(true);
        setproductToDelete(product);
    };

    if (error) {
        return (
            <div className="alert alert-danger" role="alert">
                Error: {error}
            </div>
        );
    } else if (status === 'loading') {
        <LoaderAnimation />
    } else {
        return (
            <>
                <div className="mt-5 mb-3">
                    <AddProductButton />
                    <button className='btn btn-warning btn-lg mx-2' onClick={handleRefresh}>
                        <span className='bi bi-arrow-clockwise'></span>
                        &nbsp;Refresh Products
                    </button>
                </div>
                <ProductListComponent products={products} onDelete={handleDeleteProduct} />
                <ConfirmModal show={show} title={"Confirm Delete Product"}
                    message={`Are you sure you want to delete product ${productToDelete.name}?`}
                    handleYes={() => {
                        dispatch(deleteProduct(productToDelete.id));
                        setShow(false);
                    }} handleNo={() => {
                        setShow(false);
                    }} />
            </>
        );
    }
};

export default ProductsComponent;