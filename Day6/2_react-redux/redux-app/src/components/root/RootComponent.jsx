import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import NavigationComponent from '../bs-nav/NavigationComponent';

const RootComponent = () => {
  return (
    <div className='container'>
      <Router>
        <NavigationComponent />
      </Router>
    </div>
  );
};

export default RootComponent;