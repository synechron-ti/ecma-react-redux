// import './1_collection/1_es6-map';
// import './1_collection/2_es6-set';

// import './2_objects/1_object-creation';
// import './2_objects/2_object-type';
// import './2_objects/3_object-methods';
// import './2_objects/4_custom-type';
// import './2_objects/5_using-prototype';
// import './2_objects/6_es6-class';
// import './2_objects/7_es5-properties';
// import './2_objects/8_es6-properties';
// import './2_objects/9_query-yogananda';
// import './2_objects/10_assignment';
// import './2_objects/11_inheritance';
// import './2_objects/12_es6-inheritance';

// import './3_iterators/1_custom-iterable';
// import './3_iterators/2_generators';

// import './4_modules/usage';

// import './5_promise/1_promise-creation';
// import './5_promise/2_promise-chaining';
// import './5_promise/3_promise-methods';

// import './6_ajax/dom-handler';

// import './7_beyond-es6/1_es7-features';
// import './7_beyond-es6/2_es8-object-methods';
// import './7_beyond-es6/3_es8-string-padding';
// import './7_beyond-es6/4_es8-trailing-commas';
import './7_beyond-es6/5_es9-tagged-template-literals';

