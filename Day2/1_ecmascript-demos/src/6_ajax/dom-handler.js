import postApiClient from "./post-api-client";
import getData from "./static-data";

const ajaxDiv = document.querySelector('#ajaxDiv');
const messageDiv = document.querySelector('#messageDiv');

if (ajaxDiv.style.display === 'none') {
    ajaxDiv.style.display = 'block';
    messageDiv.style.display = 'none';
}

const button = document.createElement('button');
button.className = 'btn btn-primary';
button.innerHTML = 'Load Data';

const btnArea = document.querySelector('#aDiv_btnArea');
btnArea.appendChild(button);

// // 1. Using Static Data
// button.addEventListener('click', () => {
//     // alert('Button Clicked');
//     const data = getData();
//     generateRows(data);
// });

// // 2. Using Callbacks
// button.addEventListener('click', () => {
//     postApiClient.getAllPostsUsingCallbacks((data) => {
//         generateRows(data);
//     }, (eMsg) => {
//         console.error(eMsg);
//     });
// });

// // 3. Using Promise
// button.addEventListener('click', () => {
//     postApiClient.getAllPostsUsingPromise().then((data) => {
//         generateRows(data);
//     }).catch((eMsg) => {
//         console.error(eMsg);
//     });
// });

// // 4. Using Async Await (ES 2017) 
// button.addEventListener('click', async () => {
//     try {
//         const data = await postApiClient.getAllPostsUsingPromise();
//         generateRows(data);
//     } catch (eMsg) {
//         console.error(eMsg);
//     }
// });

// // 5. Using Async Function (ES 2017) 
// button.addEventListener('click', async () => {
//     try {
//         const data = await postApiClient.getAllPostsAsync();
//         generateRows(data);
//     } catch (eMsg) {
//         console.error(eMsg);
//     }
// });

// // 6. Using Async Generators (ES 2018) 
// button.addEventListener('click', async () => {
//     const it = postApiClient.getAllPosts();

//     try {
//         const data = await it.next();
//         generateRows(data.value);
//     } catch (eMsg) {
//         console.error(eMsg);
//     }
// });

// 7. Using Async Generators with Async Iterators (ES 2018) 
button.addEventListener('click', async () => {
    const genObj = postApiClient.getPosts([1, 4, 7, 56, 88, 99]);
    let data = [];

    // console.log(genObj);

    // ECMAScript 2018 - Async Iterators
    for await (const postData of genObj) {
        if (postData !== null) {
            data.push(postData);
        } else {
            data.push({ id: 'NA', title: 'NA', body: 'NA' });
        }
    }

    generateRows(data);
});

function generateRows(data) {
    let tbody = document.querySelector('#postTable tbody');
    let row, cell;

    for (const item of data) {
        row = tbody.insertRow();
        cell = row.insertCell();
        cell.textContent = item.id;
        cell = row.insertCell();
        cell.textContent = item.title;
        cell = row.insertCell();
        cell.textContent = item.body;
    }
}