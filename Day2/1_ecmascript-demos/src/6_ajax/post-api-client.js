const url = 'https://jsonplaceholder.typicode.com/posts';

const postApiClient = {
    getAllPostsUsingCallbacks: function (successCB, errorCB) {
        fetch(url).then(response => {
            response.json().then(data => {
                successCB(data);
            }).catch(error => {
                errorCB("JSON Parse Error...");
            });
        }).catch(error => {
            errorCB("Communication Error...");
        });
    },
    getAllPostsUsingPromise: function () {
        return new Promise((resolve, reject) => {
            fetch(url).then(response => {
                response.json().then(data => {
                    resolve(data);
                }).catch(error => {
                    reject("JSON Parse Error...");
                });
            }).catch(error => {
                reject("Communication Error...");
            });
        });
    },
    getAllPostsAsync: async function () {
        try {
            var response = await fetch(url);
            if (!response.ok) {
                throw new Error('Network response was not ok.');
            }
            return await response.json();
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getAllPosts: async function* () {
        try {
            var response = await fetch(url);
            if (!response.ok) {
                throw new Error('Network response was not ok.');
            }
            yield await response.json();
        } catch (err) {
            throw new Error(err.message);
        }
    },
    // Develop a function that accepts an array of post IDs and fetches detailed information for each post asynchronously. 
    // The function must manage network requests efficiently to ensure that system performance remains optimal while 
    // maintaining usability.
    getPosts: async function* (postIds) {
        for (const postId of postIds) {
            try {
                var response = await fetch(`${url}/${postId}`);
                if (!response.ok) {
                    throw new Error(`Failed to fetch post with ID ${postId}: ${response.status}`);
                }
                const data = await response.json();
                yield data;
            } catch (error) {
                console.error(`Error fetching post with ID ${postId}: ${error.message}`);
                yield null;
            }
        }
    }
};

export default postApiClient;