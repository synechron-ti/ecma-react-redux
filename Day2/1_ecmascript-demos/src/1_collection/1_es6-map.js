'use-strict'

// let myMap = new Map();

// // console.log(myMap);
// // console.log(typeof myMap);
// // console.log(myMap.size);

// const o = { id: 1 };
// const f = function () { };

// myMap.set('the string', 'This is the value for the string key');
// myMap.set(o, 'This is the value for the object key');
// myMap.set(f, 'This is the value for the function key');

// // console.log(myMap);
// // console.log(myMap.size);

// // console.log(myMap.get('the string'));
// // console.log(myMap.get(o));
// // console.log(myMap.get(f));

// // for (const pair of myMap) {
// //     console.log(pair);
// // }

// // for (const [key, value] of myMap) {
// //     console.log(`Key: ${key}        Value: ${value}`);
// // }

// // for (const key of myMap.keys()) {
// //     console.log(`Key: ${key}`);
// // }

// for (const value of myMap.values()) {
//     console.log(`Value: ${value}`);
// }

// -----------------------------------------------------------

var phoneDirectoryMap = new Map();
phoneDirectoryMap.set('John Doe', '123-456-7890');
phoneDirectoryMap.set('Jane Doe', '098-765-4321');

// // console.log('size of the map is', phoneDirectoryMap.size);
// console.log(phoneDirectoryMap.has('John Doe'));

// // phoneDirectoryMap.delete('John Doe');
// phoneDirectoryMap.clear();
// console.log(phoneDirectoryMap.has('John Doe'));

// console.log('size of the map is', phoneDirectoryMap.size);

// const arr = Array.from(phoneDirectoryMap.values());
// const arr = [...phoneDirectoryMap.values()];
// console.log(arr);