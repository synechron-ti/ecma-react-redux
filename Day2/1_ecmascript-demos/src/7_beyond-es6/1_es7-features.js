// let result1 = Math.pow(2, 4);
// console.log(result1);

// let result2 = 2 ** 4;
// console.log(result2);

// let result3 = (-3) ** 4;
// console.log(result3);

// ----------------------------------------------

let arr = ["ReactJS", "Angular", "ExtJS"];

// // if(arr.indexOf('ReactJS')) {
// //     console.log('ReactJS is available');
// // } else {
// //     console.error('ReactJS is not available');
// // }

// // if(arr.indexOf('VueJS')) {
// //     console.log('VueJS is available');
// // } else {
// //     console.error('VueJS is not available');
// // }

// if(arr.indexOf('ReactJS') > -1) {
//     console.log('ReactJS is available');
// } else {
//     console.error('ReactJS is not available');
// }

// if(arr.indexOf('VueJS') > -1) {
//     console.log('VueJS is available');
// } else {
//     console.error('VueJS is not available');
// }

if(arr.includes('ReactJS')) {
    console.log('ReactJS is available');
} else {
    console.error('ReactJS is not available');
}

if(arr.includes('VueJS')) {
    console.log('VueJS is available');
} else {
    console.error('VueJS is not available');
}