// Case 1 - Default Import
// import square from './utils';
// console.log("Square:", square(5));

// import sqr from './utils';
// console.log("Square:", sqr(5));

// import * as utility from './utils';
// console.log("Square:", utility.default(5));

// Case 2 - Named Import
// import { square } from './utils';
// console.log("Square:", square(5));

// import { square as sqr } from './utils';
// console.log("Square:", sqr(5));

// import * as utility from './utils';
// console.log("Square:", utility.square(5));

// Case 3 - Multiple Import
// import square, { check, test } from './utils';
// console.log("Square: ", square(5));
// console.log("Check: ", check(20));
// console.log("Test: ", test(20));

// import sqr, { check as chk, test as tst } from './utils';
// console.log("Square: ", sqr(5));
// console.log("Check: ", chk(20));
// console.log("Test: ", tst(20));

// import * as utility from './utils';
// console.log("Square:", utility.default(5));
// console.log("Check: ", utility.check(20));
// console.log("Test: ", utility.test(20));

import { Queue } from './utils';

let ordersQueue = new Queue();
ordersQueue.enqueue("Order 1");
ordersQueue.enqueue("Order 2");
ordersQueue.enqueue("Order 3");

for (const order of ordersQueue) {
    console.log(order);
}