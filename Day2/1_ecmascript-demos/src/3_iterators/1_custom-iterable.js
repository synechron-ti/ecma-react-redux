'use strict';

class Queue {
    constructor() {
        this._items = [];
    }

    enqueue(item) {
        this._items.push(item);
    }

    dequeue() {
        return this._items.shift();
    }

    [Symbol.iterator]() {
        let i = 0;
        const self = this;

        return {
            next() {
                const done = i >= self._items.length;
                const value = !done ? self._items[i++] : undefined;

                return {
                    value,
                    done
                };
            }
        };
    }
}

let ordersQueue = new Queue();
ordersQueue.enqueue("Order 1");
ordersQueue.enqueue("Order 2");
ordersQueue.enqueue("Order 3");

// console.log(ordersQueue.dequeue());
// console.log(ordersQueue.dequeue());
// console.log(ordersQueue.dequeue());

for (const order of ordersQueue) {
    console.log(order);
}