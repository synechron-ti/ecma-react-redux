function firstMethod() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('first method completed...');
            resolve({ first: 'added from first method' });
        }, 2000);
    });
}

function secondMethod() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('second method completed...');
            resolve({ second: 'added from second method' });
            // reject('Something went wrong in second method');
        }, 3000);
    });
}

function thirdMethod() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('third method completed...');
            // resolve({ third: 'added from third method' });
            reject('Something went wrong in third method');
        }, 1000);
    });
}

// Promise.all([firstMethod(), secondMethod(), thirdMethod()]).then((data) => {
//     console.log(data);
// }).catch((eMsg) => {
//     console.error(eMsg);
// });

// Promise.race([firstMethod(), secondMethod(), thirdMethod()]).then((data) => {
//     console.log("Success:", data);
// }).catch((eMsg) => {
//     console.error("Error:", eMsg);
// });

// ECMAScript 2021
Promise.any([firstMethod(), secondMethod(), thirdMethod()]).then((data) => {
    console.log("Success:", data);
}).catch((eMsg) => {
    console.error("Error:", eMsg);
});