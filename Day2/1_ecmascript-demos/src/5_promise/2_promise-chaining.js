function firstMethod() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('first method completed...');
            resolve({ first: 'added from first method' });
        }, 2000);
    });
}

function secondMethod(inputData) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('second method completed...');
            resolve({ ...inputData, second: 'added from second method' });
            // reject('Something went wrong in second method');
        }, 3000);
    });
}

function thirdMethod(inputData) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('third method completed...');
            resolve({ ...inputData, third: 'added from third method' });
        }, 1000);
    });
}

firstMethod().then(secondMethod).then(thirdMethod).then((data) => {
    console.log(data);
}).catch((eMsg) => {
    console.error(eMsg);
});