// function getDataUsingCallback(callback) {
//     setTimeout(() => {
//         callback('Data received');
//     }, 5000);
// }

// getDataUsingCallback((data) => {
//     console.log(data);
// });

// function getDataUsingPromise() {
//     const promise = new Promise((resolve, reject) => {
//         // Async Code
//         setTimeout(() => {
//             resolve('Data received');
//             // reject('Data not received');
//         }, 5000);
//     });
//     return promise;
// }

// const promise = getDataUsingPromise();

// // promise.then((data) => {
// //     console.log("Success: ", data);
// // }, (error) => {
// //     console.error("Error: ", error);
// // });

// // promise.then((data) => {
// //     console.log("Success: ", data);
// // }).catch((error) => {
// //     console.error("Error: ", error);
// // });

// promise.then((data) => {
//     console.log("Success: ", data);
// }).catch((error) => {
//     console.error("Error: ", error);
// }).finally(() => {
//     console.warn("Finally always executes");
// });

// ------------------------------ Promises are resolved or rejected only once ------------------------------

function getDataUsingCallback(callback) {
    let count = 0;
    setInterval(() => {
        callback(++count);
    }, 2000);
}

getDataUsingCallback((data) => {
    console.log("Callback:", data);
});

function getDataUsingPromise() {
    let count = 0;
    return new Promise((resolve, reject) => {
        setInterval(() => {
            resolve(++count);
        }, 2000);
    });
}

getDataUsingPromise().then((data) => {
    console.log("Promise: ", data);
});