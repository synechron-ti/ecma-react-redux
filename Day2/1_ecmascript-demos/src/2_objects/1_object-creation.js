'use strict'
// let obj1 = null;
// console.log(obj1);
// console.log(typeof obj1);

// let obj2 = new Object();
// console.log(obj2);
// console.log(typeof obj2);

// let obj3 = {};
// console.log(obj3);
// console.log(typeof obj3);

// ----------------------------

const person = {
    id: 1,
    name: 'John Doe',
    address: {
        street: '123 Main Street',
        city: 'Anytown',
        state: 'AS',
        zip: '12345'
    },
    display: function () {
        console.log(this);
    }
}; 

// person.display();

// console.log(person);
// console.log(typeof person);

// Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
var person_json = JSON.stringify(person);

// console.log(person_json);
// console.log(typeof person_json);

// console.log(person.id);
// console.log(person_json.id);

// Converts a JavaScript Object Notation (JSON) string into an object.
var person_obj = JSON.parse(person_json);

console.log(person_obj);
console.log(typeof person_obj);
console.log(person_obj.id);