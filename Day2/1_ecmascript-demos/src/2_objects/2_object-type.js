'use strict';

// var toy1 = new Object();
// console.log(toy1);
// console.log(typeof toy1);

// console.log(Object.prototype);

// console.log(toy1.constructor);
// console.log(toy1.toString());

// Dunder Proto (Double Underscore Proto) - Should not be used in production code
// console.log(toy1.__proto__);
// console.log(Object.prototype);
// console.log(toy1.__proto__ === Object.prototype);

// -------------------------------- Add Property to the instance

// var toy1 = new Object();
// toy1.color = 'red';
// toy1.shape = 'square';

// // console.log(toy1);

// var toy2 = new Object();
// // console.log(toy2);

// toy2.color = 'red';
// toy2.shape = 'square';

// console.log("Toy 1 Color", toy1.color);
// console.log("Toy 1 Shape", toy1.shape);

// console.log("Toy 2 Color", toy2.color);
// console.log("Toy 2 Shape", toy2.shape);

// -------------------------------- Add Property to the instance

// var toy1 = new Object();
// var toy2 = new Object();

// Object.prototype.color = 'red';
// Object.prototype.shape = 'square';

// console.log("Toy 1 Color", toy1.color);
// console.log("Toy 1 Shape", toy1.shape);

// console.log("Toy 2 Color", toy2.color);
// console.log("Toy 2 Shape", toy2.shape);

// toy2.color = 'blue';
// toy2.shape = 'circle';

// console.log("\n");
// console.log("Toy 1 Color", toy1.color);
// console.log("Toy 1 Shape", toy1.shape);

// console.log("Toy 2 Color", toy2.color);
// console.log("Toy 2 Shape", toy2.shape);

// console.log(toy1);
// console.log(toy2);

//------------------------------------

const ename = "Manish";

// console.log(typeof ename);
// console.log(String.prototype);

String.prototype.check = function () {
    console.log("Check");
}

String.prototype.test = function () {
    console.log("Test");
}

ename.check();
ename.test();