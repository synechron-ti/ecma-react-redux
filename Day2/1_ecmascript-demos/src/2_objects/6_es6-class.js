'use strict';

// Constructor Function
class Person {
    constructor(name) {
        this._name = name;
    }

    getName() {
        return this._name;
    }

    setName(name) {
        this._name = name;
    }

    // If you use Function Expression or Arrow Function to create Member Functions
    // It will increase the memory consumption

    // 244 bytes - 122 bytes each
    // getName = function () {
    //     return this._name;
    // }

    // setName = function (name) {
    //     this._name = name;
    // }

    // 248 bytes - 124 bytes each
    // getName = () => {
    //     return this._name;
    // }

    // setName = (name) => {
    //     this._name = name;
    // }
}

var p1 = new Person("Manish");
console.log(p1.getName());
p1.setName("Abhijeet");
console.log(p1.getName());

var p2 = new Person("John");
console.log(p2.getName());
p2.setName("Ramakant");
console.log(p2.getName());

console.log(p1);
console.log(p2);

// 96 bytes - 48 bytes each
