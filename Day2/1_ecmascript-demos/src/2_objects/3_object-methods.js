'use strict';

//  // ---------------------------------------------------- Object.assign (Shallow Copy)
// let source = { id: 1, name: "Manish", address: { city: "Bangalore" } };

// let target = Object.assign({}, source);

// target.name = "Abhijeet";
// target.address.city = "Pune";

// console.log(source);
// console.log(target);

// // ---------------------------------------------------- Object.create
// // Creates a new object, using an existing object as the prototype of the newly created object.

// let source = { id: 1, name: "Manish", address: { city: "Bangalore" } };

// let assignedTarget = Object.assign({}, source);
// let createdTarget = Object.create(source);

// console.log("Source: ", source);
// console.log("Assigned Target: ", assignedTarget);
// console.log("Created Target: ", createdTarget);

// ---------------------------------------------------
// Add a New Property       - Allowed
// Delete a Property        - Allowed
// Modify Property Value    - Allowed

// let source = { id: 1, name: "Manish" };

// // Add a New Property
// source.city = "Bangalore";
// console.log(source);

// // Delete a Property
// delete source.name;
// console.log(source);

// // Modify Property Value
// source.id = 1000;
// console.log(source);

// // ---------------------------------------------------
// // Add a New Property       - Not Allowed
// // Delete a Property        - Allowed
// // Modify Property Value    - Allowed

// let source = { id: 1, name: "Manish" };

// Object.preventExtensions(source);

// if (Object.isExtensible(source)) {
//     // Add a New Property
//     source.city = "Bangalore";
//     console.log(source);
// } else {
//     console.error("New properties can not be added to the object");
// }

// // Delete a Property
// delete source.name;
// console.log(source);

// // Modify Property Value
// source.id = 1000;
// console.log(source);

// // ---------------------------------------------------
// // Add a New Property       - Not Allowed
// // Delete a Property        - Not Allowed
// // Modify Property Value    - Allowed

// let source = { id: 1, name: "Manish" };

// Object.seal(source);

// if (!Object.isSealed(source)) {
//     // Add a New Property
//     source.city = "Bangalore";
//     console.log(source);

//     // Delete a Property
//     delete source.name;
//     console.log(source);
// } else {
//     console.error("New properties can not be added to the object and existing properties can not be deleted");
// }

// // Modify Property Value
// source.id = 1000;
// console.log(source);

// ---------------------------------------------------
// Add a New Property       - Not Allowed
// Delete a Property        - Not Allowed
// Modify Property Value    - Not Allowed

let source = { id: 1, name: "Manish" };

Object.freeze(source);

if (!Object.isFrozen(source)) {
    // Add a New Property
    source.city = "Bangalore";
    console.log(source);

    // Delete a Property
    delete source.name;
    console.log(source);

    // Modify Property Value
    source.id = 1000;
    console.log(source);
} else {
    console.error("Object is frozen, no operations are allowed on the object");
}

