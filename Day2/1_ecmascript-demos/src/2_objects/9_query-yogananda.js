'use strict';

// Constructor Function
const Person = (function () {
    let priv = new WeakMap();
    var _ = function (instance) { return priv.get(instance); };

    class PersonClass {
        constructor(name, age) {
            var privateMembers = { name: name, age: age };
            priv.set(this, privateMembers);
        }

        // Accessor Property
        get Name() {
            return _(this).name;
        }

        set Name(name) {
            _(this).name = name;
        }

        get Age() {
            return _(this).age;
        }

        set Age(age) {
            _(this).age = age;
        }
    }

    return PersonClass;
})();

var p1 = new Person("Manish", 0);
console.log(p1.Name);               // Get
console.log(p1.Age);                // Get

p1.Name = "Abhijeet";               // Set
p1.Age = 30;                        // Set

console.log(p1.Name);               // Get
console.log(p1.Age);                // Get
