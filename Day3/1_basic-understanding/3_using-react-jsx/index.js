// const appElement = document.querySelector('#app');
// const hOneElement = document.createElement('h1');
// hOneElement.className = 'orange';
// hOneElement.textContent = 'Hello, World!';
// appElement.appendChild(hOneElement);

// -------------------------------------------------------------------------------------------- React 17
// const hOneElement = React.createElement('h1', { className: 'orange' }, 'Hello, World!');
// ReactDOM.render(hOneElement, document.querySelector('#app'));

// -------------------------------------------------------------------------------------------- React 18
// const hOneElement = React.createElement('h1', { className: 'orange' }, 'Hello, World!');
// const root = ReactDOM.createRoot(document.querySelector('#app'));
// root.render(hOneElement); 

// -------------------------------------------------------------------------------------------- React JSX
const HOneComponent = () => {
    return (
        <h1 className="orange">
            Hello, World!
        </h1>
    );
}

const root = ReactDOM.createRoot(document.querySelector('#app'));
root.render(<HOneComponent />);