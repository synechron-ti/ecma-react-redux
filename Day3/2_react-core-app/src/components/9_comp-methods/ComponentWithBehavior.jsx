import React, { Component } from 'react';

// class ComponentWithBehavior extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { id: 1, count: 0 };
//     }

//     handleClick() {
//         // alert("Button Clicked");
//         // console.log(this);

//         // Not Correct
//         // this.state.count += 1;
//         // console.log(this.state);

//         // If you want to update the UI, use setState to update the state
//         // this.setState({count: this.state.count + 1});
//         // console.log(this.state);

//         // setState is Async, so if you want to excute something after the state is updated, use callback function
//         this.setState({ count: this.state.count + 1 }, () => {
//             console.log(this.state);
//         });
//     }

//     render() {
//         return (
//             <div className='text-center'>
//                 <h2 className="text-primary">Component with Behavior</h2>
//                 <h2 className="text-primary">Id: {this.state.id}</h2>
//                 <h2 className="text-primary">Count: {this.state.count}</h2>

//                 <div className="d-grid gap-2 col-6 mx-auto mt-5">
//                     <button className='btn btn-outline-primary' onClick={this.handleClick.bind(this)}>
//                         Click
//                     </button>
//                 </div>
//             </div>
//         );
//     }
// }

// const ComponentWithBehavior = () => {
//     const [state, setState] = React.useState({ id: 1, count: 0 });
//     return (
//         <div className='text-center'>
//             <h2 className="text-primary">Component with Behavior</h2>
//             <h2 className="text-primary">Id: {state.id}</h2>
//             <h2 className="text-primary">Count: {state.count}</h2>

//             <div className="d-grid gap-2 col-6 mx-auto mt-5">
//                 <button className='btn btn-outline-primary' onClick={() => { setState({ id: 1, count: state.count + 1 }) }}>
//                     Click
//                 </button>
//             </div>
//         </div>
//     );
// };

const ComponentWithBehavior = () => {
    const [id, setId] = React.useState(1);
    const [count, setCount] = React.useState(0);

    return (
        <div className='text-center'>
            <h2 className="text-primary">Component with Behavior</h2>
            <h2 className="text-primary">Id: {id}</h2>
            <h2 className="text-primary">Count: {count}</h2>

            <div className="d-grid gap-2 col-6 mx-auto mt-5">
                <button className='btn btn-outline-primary' onClick={() => { setCount(count + 1); }}>
                    Click
                </button>
            </div>
        </div>
    );
};

export default ComponentWithBehavior;