// import React from 'react';

// class HelloComponent extends React.Component {
//     render() {
//         return (
//             <h1>Hello World!</h1>
//         );
//     }
// }

// export default HelloComponent;

// // ------------------------------------------------------

// import React, { Component } from 'react';

// class HelloComponent extends Component {
//     render() {
//         return (
//             <h1>Hello World!</h1>
//         );
//     }
// }

// export default HelloComponent;

// // ------------------------------------------------------

// import React, { Component } from 'react';

// class HelloComponent extends Component {
//     render() {
//         return (
//             <div>
//                 <h1>Hello World!</h1>
//                 <h1>Hello World Again!</h1>
//             </div>
//         );
//     }
// }

// export default HelloComponent;

// // ------------------------------------------------------

// // import React, { Component } from 'react';
// // import React, { Component, Fragment } from 'react';
// import React, { Component } from 'react';

// class HelloComponent extends Component {
//     render() {
//         return (
//             // <React.Fragment>
//             //     <h1>Hello World!</h1>
//             //     <h1>Hello World Again!</h1>
//             // </React.Fragment>
//             // <Fragment>
//             //     <h1>Hello World!</h1>
//             //     <h1>Hello World Again!</h1>
//             // </Fragment>
//             <>
//                 <h1>Hello World!</h1>
//                 <h1>Hello World Again!</h1>
//             </>
//         );
//     }
// }

// export default HelloComponent;


// ------------------------------------------------------

// function HelloComponent() {
//     return (
//         <div>
//             <h1>Hello World!</h1>
//             <h1>Function Declaration!</h1>
//         </div>
//     );
// }

// const HelloComponent = function () {
//     return (
//         <div>
//             <h1>Hello World!</h1>
//             <h1>Function Expression!</h1>
//         </div>
//     );
// }

// const HelloComponent = () => {
//     return (
//         <div>
//             <h1>Hello World!</h1>
//             <h1>Arrow Function Multiline!</h1>
//         </div>
//     );
// }

// const HelloComponent = () => (
//     <div>
//         <h1>Hello World!</h1>
//         <h1>Arrow Function Singleline!</h1>
//     </div>
// );

// export default HelloComponent;

// ---------------------------------------------------------- Using Global CSS

const HelloComponent = () => (
    <div className="container-fluid">
        <h1 className="red">Hello World!</h1>
        <h1 className="text-primary">Arrow Function Singleline!</h1>
        <h2 className="text-success">
            Activity Icon
            <i className="bi bi-emoji-smile"></i>
            <i className="bi bi-activity"></i>
        </h2>
    </div>
);

export default HelloComponent;