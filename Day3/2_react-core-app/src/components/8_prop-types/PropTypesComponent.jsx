// import React, { Component } from 'react';

// // class PropTypesComponent extends Component {
// //     constructor(props) {
// //         super(props);
// //         console.log(this.props);
// //     }

// //     render() {
// //         return (
// //             <div>
// //                 <h1>Default Props</h1>
// //                 <h2 className='text-primary'>Hello, {this.props.name.toUpperCase()}</h2>
// //                 <h2 className='text-primary'>You are from, {this.props.city} </h2>
// //             </div>
// //         );
// //     }

// //     // static get defaultProps() {
// //     //     return {
// //     //         name: "Manish",
// //     //         city: "Pune"
// //     //     };
// //     // }
// // }

// // PropTypesComponent.defaultProps = {
// //     name: "Manish",
// //     city: "Pune"
// // };

// const PropTypesComponent = ({ name = "Manish", city = "Pune" }) => {
//     return (
//         <div>
//             <h1>Default Props</h1>
//             <h2 className='text-primary'>Hello, {name.toUpperCase()}</h2>
//             <h2 className='text-primary'>You are from, {city} </h2>
//         </div>
//     );
// };

// ----------------------------------------------------------
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// class PropTypesComponent extends Component {
//     render() {
//         return (
//             <div>
//                 <h1>Props Validation</h1>
//                 <h2 className='text-primary'>Hello, {this.props.name.toUpperCase()}</h2>
//                 <h2 className='text-primary'>You are from, {this.props.city} </h2>
//             </div>
//         );
//     }

//     // static get propTypes() {
//     //     return {
//     //         name: PropTypes.string.isRequired,
//     //         city: PropTypes.string
//     //     };
//     // }
// }

// PropTypesComponent.propTypes = {
//     name: PropTypes.string.isRequired,
//     city: PropTypes.string
// };

const PropTypesComponent = ({ name, city, products }) => {
    return (
        <div>
            <h1>Props Validation</h1>
            <h2 className='text-primary'>Hello, {name.toUpperCase()}</h2>
            <h2 className='text-primary'>You are from, {city} </h2>
        </div>
    );
}

PropTypesComponent.propTypes = {
    name: PropTypes.string.isRequired,
    city: PropTypes.string,
    products: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    }))
};

class PropTypesRoot extends Component {
    render() {
        return (
            <div>
                <PropTypesComponent name={"Abhijeet"} city={"Mumbai"} />
            </div>
        );
    }
}

export default PropTypesRoot;