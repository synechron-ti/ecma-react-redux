// import React, { Component } from 'react';

// class ComponentWithState extends Component {
//     constructor() {
//         super();

//         // State must be initialized in the constructor
//         // State must be set to an object or null
//         // this.state = "Synechron";            // Error 

//         this.state = { name: "Synechron" };
//         this.message = "Hello from ComponentWithState";

//         console.log("Ctor, State:", this.state);
//     }

//     render() {
//         console.log("Render, State:", this.state);

//         return (
//             <div>
//                 <h2 className="text-primary">Component With State</h2>
//                 <h2 className="text-primary">Hello, {this.state.name}</h2>
//                 <h2 className="text-primary">{this.message}</h2>
//             </div>
//         );
//     }
// }

// export default ComponentWithState;

import React, { useState } from 'react';

const ComponentWithState = () => {
    const [name, setName] = useState("Synechron");
    const message = "Hello from ComponentWithState"

    return (
        <div>
            <h2 className="text-primary">Component With State</h2>
            <h2 className="text-primary">Hello, {name}</h2>
            <h2 className="text-primary">{message}</h2>
        </div>
    );
};

export default ComponentWithState;