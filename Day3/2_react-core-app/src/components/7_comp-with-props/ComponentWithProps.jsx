// import React, { Component } from 'react';

// class ComponentWithProps extends Component {
//     constructor(props) {
//         super(props);

//         // this.props = { name: "Synechron" };         // Props cannot be initialized in the constructor
//         // this.props.name = "Abhijeet";               // Error - Props cannot be modified, they are immutable & read-only  

//         // Refrence Copy
//         // this.state = this.props;
//         // this.state.name = "Abhijeet";

//         // Shallow Copy - Will work only at first level, it will allow to modify the nested objects
//         // this.state = { ...this.props };
//         // this.state.name = "Abhijeet";
//         // this.state.address.city = "Mumbai";

//         // Deep Copy - Will work at all levels
//         this.state = JSON.parse(JSON.stringify(this.props));
//         this.state.name = "Abhijeet";
//         this.state.address.city = "Mumbai";

//         // Immutable should be handled using immutable.js or immer.js

//         console.log("Ctor, State:", this.state);
//         console.log("Ctor, Props:", this.props);
//     }

//     render() {
//         console.log("Render, State:", this.state);
//         console.log("Render, Props:", this.props);

//         return (
//             <div>
//                 <h2 className="text-primary">Component with Props</h2>
//             </div>
//         );
//     }
// }

// export default ComponentWithProps;

import React, { useState } from 'react';

const ComponentWithProps = (props) => {
    console.log(props);

    // Never modify props, they are immutable & read-only
    // props.name = "Abhijeet";
    // props.address.city = "Mumbai";

    // const [state, setState] = useState({ ...props });
    const [state, setState] = useState(JSON.parse(JSON.stringify(props)));

    state.name = "Abhijeet";
    state.address.city = "Mumbai";

    return (
        <div>
            <h2 className="text-primary">Component with Props</h2>
        </div>
    );
};

export default ComponentWithProps;