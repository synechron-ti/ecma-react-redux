import React from 'react';
// import ComponentOne from '../2_multi-components/ComponentOne';
// import ComponentTwo from '../2_multi-components/ComponentTwo';

// import ComponentOne from '../3_inline-style/ComponentOne';
// import ComponentTwo from '../3_inline-style/ComponentTwo';

// import ComponentOne from '../4_external-css/comp-one/ComponentOne';
// import ComponentTwo from '../4_external-css/comp-two/ComponentTwo';

// import ComponentOne from '../5_css-modules/comp-one/ComponentOne';
// import ComponentTwo from '../5_css-modules/comp-two/ComponentTwo';

import ComponentWithState from '../6_comp-with-state/ComponentWithState';
import ComponentWithProps from '../7_comp-with-props/ComponentWithProps';
import PropTypesRoot from '../8_prop-types/PropTypesComponent';
import ComponentWithBehavior from '../9_comp-methods/ComponentWithBehavior';
import EventComponent from '../10_synthetic-events/EventComponent';
import CounterAssignment from '../11_assignment/CounterAssignment';

const RootComponent = () => {
    return (
        <div className='container'>
            {/* <ComponentOne />
            <ComponentTwo /> */}

            {/* <ComponentWithState /> */}
            {/* <ComponentWithProps id={1} name={"Manish"} address={{ city: "Pune", state: "MH" }} display={() => { alert("From Root"); }} /> */}
            {/* <PropTypesRoot /> */}
            {/* <ComponentWithBehavior /> */}
            {/* <EventComponent /> */}
            <CounterAssignment />
        </div>
    );
};

export default RootComponent;