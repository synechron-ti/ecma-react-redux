const url = process.env.REACT_APP_POSTS_API_URL;

const postApiClient = {
    getAllPosts: function () {
        return new Promise((resolve, reject) => {
            fetch(url).then(response => {
                response.json().then(data => {
                    resolve(data);
                }).catch(error => {
                    reject("JSON Parse Error...");
                });
            }).catch(error => {
                reject("Communication Error...");
            });
        });
    }
};

export default postApiClient;