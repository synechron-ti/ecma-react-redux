// import TextInput from "../common/TextInput";

// const FormComponent = (props) => {
//     return (
//         <div className='row'>
//             <div className='col-sm-6 offset-sm-3'>
//                 <form className='form-horizontal' autoComplete='off' onSubmit={
//                     (e) => {
//                         e.preventDefault();
//                         props.saveEmployee(props.employee);
//                     }
//                 }>
//                     <fieldset disabled={props.disabled}>
//                         <legend className="text-center text-secondary text-uppercase font-weight-bold">Add/Edit Employee Information</legend>
//                         <hr className="mt-0" />
//                         <TextInput label={"Employee Id"} name={"id"} value={props.employee.id} readOnly={true} />
//                         <TextInput label={"Employee Name"} name={"name"} value={props.employee.name} onChange={props.changeEmployee} />
//                         <TextInput label={"Designation"} name={"designation"} value={props.employee.designation} onChange={props.changeEmployee} />
//                         <TextInput label={"Salary"} name={"salary"} value={props.employee.salary} onChange={props.changeEmployee} />

//                         <div className="d-grid gap-2 mx-auto mt-3">
//                             <button type="submit" className="btn btn-primary">Submit</button>
//                             <button type="reset" className="btn btn-warning" onClick={
//                                 (e) => { props.resetEmployee(); }
//                             }>Reset</button>
//                         </div>
//                     </fieldset>
//                 </form>
//             </div>
//         </div>
//     );
// }

// export default FormComponent;

// // -------------------------------------------------------- Using Hook Form
// import React, { useEffect } from 'react';
// import { useForm } from 'react-hook-form';

// const FormComponent = ({ employee, saveEmployee, resetEmployee, disabled }) => {
//     const { register, handleSubmit, reset, formState: { errors } } = useForm({
//         defaultValues: employee,
//     });

//     useEffect(() => {
//         reset(employee);
//     }, [employee, reset]);

//     const onSubmit = (data) => {
//         saveEmployee(data);
//     }

//     return (
//         <div className='row'>
//             <div className='col-sm-6 offset-sm-3'>
//                 <form className='form-horizontal' autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
//                     <fieldset disabled={disabled}>
//                         <legend className="text-center text-secondary text-uppercase font-weight-bold">Add/Edit Employee Information</legend>
//                         <hr className="mt-0" />

//                         <div className="form-group">
//                             <label htmlFor="id">Employee Id</label>
//                             <input type="text" className="form-control" id="id" {...register('id')} readOnly />
//                         </div>

//                         <div className="form-group">
//                             <label htmlFor="name">Employee Name</label>
//                             <input type="text" className={`form-control ${errors.name ? 'is-invalid' : ''}`} id="name" {...register('name', { required: true })} />
//                             {errors.name && <div className="invalid-feedback">Employee Name is required</div>}
//                         </div>

//                         <div className="form-group">
//                             <label htmlFor="designation">Designation</label>
//                             <input type="text" className={`form-control ${errors.designation ? 'is-invalid' : ''}`} id="designation" {...register('designation', { required: true })} />
//                             {errors.designation && <div className="invalid-feedback">Designation is required</div>}
//                         </div>

//                         <div className="form-group">
//                             <label htmlFor="salary">Salary</label>
//                             <input type="text" className={`form-control ${errors.salary ? 'is-invalid' : ''}`} id="salary" {...register('salary', {
//                                 required: true,
//                                 validate: value => !isNaN(value) || "Salary must be a number"
//                             })} />
//                             {errors.salary && <div className="invalid-feedback">{errors.salary.message}</div>}
//                         </div>

//                         <div className="d-grid gap-2 mx-auto mt-3">
//                             <button type="submit" className="btn btn-primary">Submit</button>
//                             <button type="reset" className="btn btn-warning" onClick={resetEmployee}>Reset</button>
//                         </div>
//                     </fieldset>
//                 </form>
//             </div>
//         </div >
//     );
// }

// export default FormComponent;

// -------------------------------------------------------- Using Hook Form
import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import TextInputHookForm from '../common/TextInputHookForm';

const FormComponent = ({ employee, saveEmployee, resetEmployee, disabled }) => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm({
        defaultValues: employee,
    });

    useEffect(() => {
        reset(employee);
    }, [employee, reset]);

    const onSubmit = (data) => {
        saveEmployee(data);
    }

    return (
        <div className='row'>
            <div className='col-sm-6 offset-sm-3'>
                <form className='form-horizontal' autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
                    <fieldset disabled={disabled}>
                        <legend className="text-center text-secondary text-uppercase font-weight-bold">Add/Edit Employee Information</legend>
                        <hr className="mt-0" />

                        <TextInputHookForm label={"Employee Id"} name={"id"} readOnly={true} register={register} errors={errors} />

                        <TextInputHookForm label={"Employee Name"} name={"name"} register={register} errors={errors} validation={{ required: 'Employee Name is required' }} />

                        <TextInputHookForm label={"Designation"} name={"designation"} register={register} errors={errors} validation={{ required: 'Designation is required' }} />
                        
                        <TextInputHookForm label={"Salary"} name={"salary"} register={register} errors={errors} validation={{
                            required: 'Salary is required',
                            validate: value => !isNaN(value) || "Salary must be a number"
                        }} />

                        <div className="d-grid gap-2 mx-auto mt-3">
                            <button type="submit" className="btn btn-primary">Submit</button>
                            <button type="reset" className="btn btn-warning" onClick={resetEmployee}>Reset</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div >
    );
}

export default FormComponent;