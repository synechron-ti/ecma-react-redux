import React from 'react';
import CrudAssignment from '../1_assignment/CrudAssignment';
import ErrorHandler from '../common/ErrorHandler';
import AjaxComponent from '../2_ajax/AjaxComponent';

const RootComponent = () => {
    return (
        <div className='container'>
            <ErrorHandler>
                {/* <CrudAssignment /> */}
                <AjaxComponent />
            </ErrorHandler>
        </div>
    );
};

export default RootComponent;