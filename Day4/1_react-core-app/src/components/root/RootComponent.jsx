import React from 'react';
// import CounterAssignment from '../1_assignment/CounterAssignment';
import CounterAssignment from '../2_parent-child-communication/CounterAssignment';
import HookRoot from '../3_hooks/hook-root';
import ControlledAndUncontrolledComponent from '../4_controlled-uncontrolled/ControlledAndUncontrolledComponent';
import CalculatorAssignment from '../5_assignment/CalculatorAssignment';
import ListRoot from '../6_working-with-arrays/ListComponent';
import SiblingCommunication from '../7_managing-data/without-context/SiblingCommunication';
import PropDrillingParent from '../7_managing-data/without-context/PropDrilling';
import WithContextParent from '../7_managing-data/with-context/WithContext';
import MultiContextParent from '../7_managing-data/with-context/MultiContext';
import SiblingCommunicationUsingContext from '../7_managing-data/with-context/SiblingCommunication';
import CodeStructuringParent from '../7_managing-data/with-context/code-structuring/CodeStructuringParent';

const RootComponent = () => {
    return (
        <div className='container'>
            {/* <CounterAssignment /> */}
            {/* <CounterAssignment /> */}
            {/* <HookRoot /> */}
            {/* <ControlledAndUncontrolledComponent /> */}
            {/* <CalculatorAssignment /> */}
            {/* <ListRoot /> */}

            {/* <SiblingCommunication /> */}
            {/* <PropDrillingParent /> */}

            {/* <WithContextParent /> */}
            {/* <MultiContextParent /> */}
            {/* <SiblingCommunicationUsingContext /> */}
            <CodeStructuringParent />
        </div>
    );
};

export default RootComponent;