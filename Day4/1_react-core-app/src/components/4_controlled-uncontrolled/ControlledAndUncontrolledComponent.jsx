import React, { Component } from 'react';

class ControlledAndUncontrolledComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { name: "Manish" };
        this.inputRef = React.createRef();
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(e) {
        this.setState({ name: e.target.value });
    }

    handleClick(e) {
        if(this.inputRef.current.value) {
            this.setState({ name: this.inputRef.current.value });
        }
    }

    render() {
        return (
            <div className='text-center mt-5'>
                <h1 className="text-primary mb-5">Controlled & Uncontrolled Component</h1>
                {/* This concept applies only when you are using editable elements in HTML */}
                {/* textbox, checkbox, dropdowns, radio, text area */}
                <div className="d-grid gap-2 col-6 mx-auto mt-5">
                    {/* Uncontrolled */}
                    {/* <input type="text" className='form-control' />
                    <input type="text" className='form-control' defaultValue={"Abhijeet"} />
                    <input type="text" className='form-control' defaultValue={this.state.name} /> */}

                    {/* Controlled */}
                    {/* <input type="text" className='form-control' value={"Abhijeet"} readOnly />
                    <input type="text" className='form-control' value={this.state.name} readOnly />
                    <input type="text" className='form-control' value={this.state.name} onChange={this.handleChange} /> */}

                    {/* <input type="text" className='form-control' value={this.state.name} onChange={this.handleChange} />
                    <h2 className="text-info">Hello, {this.state.name}</h2> */}

                    <input type="text" className='form-control' defaultValue={this.state.name} ref={this.inputRef}/>
                    <button className='btn btn-primary' onClick={this.handleClick}>Say Hello</button>
                    <h2 className="text-info">Hello, {this.state.name}</h2>
                </div>
            </div>
        );
    }
}

export default ControlledAndUncontrolledComponent;