// import React, { Component } from 'react';
// import PropTypes from 'prop-types';

// class Counter extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { count: 0, flag: false };
//         this.clickCount = 0;
//         this.inc = this.inc.bind(this);
//         this.dec = this.dec.bind(this);
//         this.reset = this.reset.bind(this);
//     }

//     manageClickCount() {
//         this.clickCount++;
//         if (this.clickCount > 9) {
//             this.setState({ flag: true }, () => {
//                 if (this.props.onMax) {
//                     this.props.onMax(this.state.flag);
//                 }
//             });
//         }
//     }

//     inc() {
//         this.setState({ count: this.state.count + this.props.interval }, () => {
//             this.manageClickCount();
//         });
//     }

//     dec() {
//         this.setState({ count: this.state.count - this.props.interval }, () => {
//             this.manageClickCount();
//         });
//     }

//     reset() {
//         this.clickCount = 0;
//         this.setState({ count: 0, flag: false }, () => {
//             if (this.props.onMax) {
//                 this.props.onMax(this.state.flag);
//             }
//         });
//     }

//     render() {
//         return (
//             <>
//                 <div className="text-center">
//                     <h3 className="text-info">Counter Component</h3>
//                 </div>
//                 <div className="d-grid gap-2 mx-auto col-6">
//                     <input type="text" className="form-control form-control-lg" value={this.state.count} readOnly />
//                     <button className="btn btn-info" disabled={this.state.flag} onClick={this.inc}>
//                         <span className='fs-4'>+</span>
//                     </button>
//                     <button className="btn btn-info" disabled={this.state.flag} onClick={this.dec}>
//                         <span className='fs-4'>-</span>
//                     </button>
//                     <button className="btn btn-secondary" disabled={!this.state.flag} onClick={this.reset}>
//                         <span className='fs-4'>Reset</span>
//                     </button>
//                 </div>
//             </>
//         );
//     }

//     static get defaultProps() {
//         return {
//             interval: 1
//         };
//     }

//     static get propTypes() {
//         return {
//             interval: PropTypes.number,
//             onMax: PropTypes.func
//         };
//     }
// }

// class CounterAssignment extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { message: '' };
//         this.p_reset = this.p_reset.bind(this);
//         this.c = React.createRef();
//         this.updateMessage = this.updateMessage.bind(this);
//     }

//     updateMessage(flag) {
//         if (flag)
//             this.setState({ message: 'Max click reached, please reset to continue...' });
//         else
//             this.setState({ message: '' });
//     }

//     p_reset(e) {
//         // console.log(this.refs.c);       // Deprecated
//         // console.log(this.c);

//         if (this.c.current) {
//             this.c.current.reset();
//         }
//     }

//     render() {
//         return (
//             <div>
//                 <h2 className="text-success text-center mt-3 mb-3">Calling Parent Method from Child using callbacks</h2>
//                 {
//                     this.state.message && (
//                         <div className="d-grid gap-2 mx-auto col-9 mt-2 text-center">
//                             <div className="alert alert-danger mt-1 mb-1 fs-5" role="alert">
//                                 {this.state.message}
//                             </div>
//                         </div>
//                     )
//                 }

//                 <Counter ref={this.c} onMax={this.updateMessage} />

//                 <div className="d-grid gap-2 mx-auto col-6 mt-5">
//                     <button className="btn btn-warning" onClick={this.p_reset}>
//                         <span className='fs-4'>Parent Reset Button</span>
//                     </button>
//                 </div>

//                 {/* <h2 className="text-success text-center mt-3 mb-3">Calling Child Method from Parent using ref</h2> */}
//                 {/* <Counter ref="c" /> */}  {/* Deprecated */}
//                 {/* <Counter ref={this.c} />

//                 <div className="d-grid gap-2 mx-auto col-6 mt-5">
//                     <button className="btn btn-warning" onClick={this.p_reset}>
//                         <span className='fs-4'>Parent Reset Button</span>
//                     </button>
//                 </div> */}
//             </div>
//         );
//     }
// }

// export default CounterAssignment;


// -------------------------------------------------
import React, { useState, useCallback, useRef, forwardRef, useImperativeHandle, useEffect } from 'react';
import PropTypes from 'prop-types';

const Counter = forwardRef(function ({ interval = 1, onMax }, ref) {
    const [count, setCount] = useState(0);
    const [flag, setFlag] = useState(false);
    const clickCount = useRef(0);

    const manageClickCount = useCallback(() => {
        clickCount.current++;
        if (clickCount.current > 9) {
            setFlag(true);
        }
    }, []);

    useEffect(() => {
        if (onMax) {
            onMax(flag);
        }
    }, [flag, onMax]);

    const inc = useCallback(() => {
        setCount(prevCount => prevCount + interval);
        manageClickCount();
    }, [interval, manageClickCount]);

    const dec = useCallback(() => {
        setCount(prevCount => prevCount - interval);
        manageClickCount();
    }, [interval, manageClickCount]);

    const reset = useCallback(() => {
        setCount(0);
        setFlag(false);
        clickCount.current = 0;
    }, []);

    useImperativeHandle(ref, () => ({
        reset
    }));

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component - Functional</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={count} readOnly />
                <button className="btn btn-info" disabled={flag} onClick={inc}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info" disabled={flag} onClick={dec}>
                    <span className='fs-4'>-</span>
                </button>
                <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
                    <span className='fs-4'>Reset</span>
                </button>
            </div>
        </>
    );
});

Counter.propTypes = {
    interval: PropTypes.number,
    onMax: PropTypes.func
};

const CounterAssignment = () => {
    const [message, setMessage] = useState('');
    const c = useRef();

    const updateMessage = useCallback((flag) => {
        if (flag)
            setMessage('Max click reached, please reset to continue...');
        else
            setMessage('');
    }, []);

    const p_reset = (e) => {
        if (c.current) {
            c.current.reset();
        }
    }

    return (
        <div>
            <h2 className="text-success text-center mt-3 mb-3">Calling Parent Method from Child using callbacks</h2>

            {
                message && (
                    <div className="d-grid gap-2 mx-auto col-9 mt-2 text-center">
                        <div className="alert alert-danger mt-1 mb-1 fs-5" role="alert">
                            {message}
                        </div>
                    </div>
                )
            }

            <Counter ref={c} onMax={updateMessage} />

            <div className="d-grid gap-2 mx-auto col-6 mt-5">
                <button className="btn btn-warning" onClick={p_reset}>
                    <span className='fs-4'>Parent Reset Button</span>
                </button>
            </div>
        </div>
    );
};

export default CounterAssignment;