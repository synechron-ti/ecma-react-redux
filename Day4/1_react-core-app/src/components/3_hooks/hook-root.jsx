import React from 'react';
import EffectHookDemo from './1_effect-hook';
import CallbackAndMemoHookDemo from './2_callback-memo-hook';

const HookRoot = () => {
    return (
        <>
            {/* <EffectHookDemo /> */}
            <CallbackAndMemoHookDemo />
        </>
    );
};

export default HookRoot;