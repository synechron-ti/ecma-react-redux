import React, { useCallback, useEffect, useMemo, useState } from 'react';

// It is a HOC, that prevents a component from re-rendering 
// if the props (or values within it) have not changed.
const Counter = React.memo((props) => {
    console.log('Counter Component Rendered...', props.children);

    return (
        <div>
            <h4>Value: {props.value}</h4>
            <button className="btn btn-primary" onClick={props.handleClick}>
                {props.children}
            </button>
        </div>
    );
});

const SimpleComponent = () => {
    // const data = { name: 'Synechron' };
    // const data = useMemo(() => {
    //     return { name: 'Synechron' };
    // }, []);

    const [num, setNum] = useState(0);
    
    const data = useMemo(() => {
        return { 
            name: 'Manish',
            salary: 10 * num
        };
    }, [num]);

    useEffect(() => {
        console.log(`SimpleComponent Effect Hook Executed...`);
    }, [data]);

    return (
        <div>
            <h2>Simple Component</h2>
            <h3>{data.name}</h3>
            <h3>{data.salary}</h3>
            <h3>Number: {num}</h3>
            <button className="btn btn-primary" onClick={() => setNum(num + 1)}>
                Change Number
            </button>
        </div>
    );
};

const CallbackAndMemoHookDemo = () => {
    const [incrementValue, setIncrementValue] = React.useState(0);
    const [multiplyValue, setMultiplyValue] = React.useState(2);

    // const increment = () => {
    //     setIncrementValue(incrementValue + 1);
    // }

    // const multiply = () => {
    //     setMultiplyValue(multiplyValue * 2);
    // }

    const increment = useCallback(() => {
        setIncrementValue(incrementValue + 1);
    }, [incrementValue])

    const multiply = useCallback(() => {
        setMultiplyValue(multiplyValue * 2);
    }, [multiplyValue]);

    return (
        <div>
            <h2>Callback and Memo Hook Demo</h2>
            <Counter handleClick={increment} value={incrementValue}>
                Increment
            </Counter>
            <hr />
            <Counter handleClick={multiply} value={multiplyValue}>
                Multiply
            </Counter>
            <hr />
            <SimpleComponent />
        </div>
    );
};

export default CallbackAndMemoHookDemo;

// Recreation of Functions:
// Without useCallback(), the increment and ,multiply functions will be recreated on every render.
// This could be inefficient because these functions are passed as props to Counter component,
// causing unnecessary re-renders of those components.
