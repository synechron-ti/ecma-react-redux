import React, { Component } from 'react';

export const CounterContext = React.createContext();

class CounterContextProvider extends Component {
    constructor(props) {
        super(props);
        this.state = { currentCount: 0 };
        this.updateCount = this.updateCount.bind(this);
    }

    updateCount(count) {
        this.setState({ currentCount: count });
    }

    render() {
        return (
            <CounterContext.Provider value={{ count: this.state.currentCount, setCount: this.updateCount }}>
                {this.props.children}
            </CounterContext.Provider>
        );
    }
}

export default CounterContextProvider;