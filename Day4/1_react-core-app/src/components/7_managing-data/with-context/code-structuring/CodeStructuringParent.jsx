import React, { Component } from 'react';
import Counter from './Counter';
import CounterSibling from './CounterSibling';
import CounterContextProvider from './ConterContext';

class CodeStructuringParent extends Component {
    render() {
        return (
            <CounterContextProvider>
                <Counter />
                <hr />
                <CounterSibling />
            </CounterContextProvider>
        );
    }
}

export default CodeStructuringParent;