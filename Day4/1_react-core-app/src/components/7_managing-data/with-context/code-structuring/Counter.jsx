import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CounterContext } from './ConterContext';

class Counter extends Component {
    static contextType = CounterContext;

    constructor(props) {
        super(props);
        this.inc = this.inc.bind(this);
        this.dec = this.dec.bind(this);
    }

    inc(e) {
        const { count, setCount } = this.context;
        setCount(count + this.props.interval);
    }

    dec(e) {
        const { count, setCount } = this.context;
        setCount(count - this.props.interval);
    }

    render() {
        return (
            <>
                <div className="text-center">
                    <h3 className="text-info">Counter Component</h3>
                </div>
                <div className="d-grid gap-2 mx-auto col-6">
                    <input type="text" className="form-control form-control-lg" value={this.context.count} readOnly />
                    <button className="btn btn-info"
                        onClick={this.inc}>
                        <span className='fs-4'>+</span>
                    </button>
                    <button className="btn btn-info"
                        onClick={this.dec}>
                        <span className='fs-4'>-</span>
                    </button>
                </div>
            </>
        );
    }

    static get propTypes() {
        return {
            interval: PropTypes.number
        };
    }

    static get defaultProps() {
        return {
            interval: 1
        };
    }
}

export default Counter;