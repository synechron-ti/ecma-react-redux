import React, { Component } from 'react';
import PropTypes from 'prop-types';

const CounterContext = React.createContext();

class Counter extends Component {
    static contextType = CounterContext;

    constructor(props) {
        super(props);
        this.inc = this.inc.bind(this);
        this.dec = this.dec.bind(this);
    }

    inc(e) {
        const { count, setCount } = this.context;
        setCount(count + this.props.interval);
    }

    dec(e) {
        const { count, setCount } = this.context;
        setCount(count - this.props.interval);
    }

    render() {
        return (
            <>
                <div className="text-center">
                    <h3 className="text-info">Counter Component</h3>
                </div>
                <div className="d-grid gap-2 mx-auto col-6">
                    <input type="text" className="form-control form-control-lg" value={this.context.count} readOnly />
                    <button className="btn btn-info"
                        onClick={this.inc}>
                        <span className='fs-4'>+</span>
                    </button>
                    <button className="btn btn-info"
                        onClick={this.dec}>
                        <span className='fs-4'>-</span>
                    </button>
                </div>
            </>
        );
    }

    static get propTypes() {
        return {
            interval: PropTypes.number
        };
    }

    static get defaultProps() {
        return {
            interval: 1
        };
    }
}

class CounterSibling extends Component {
    static contextType = CounterContext;

    render() {
        return (
            <div className="text-center">
                <h3 className="text-success">Counter Sibling Component</h3>
                <h2>Current Count is: {this.context.count}</h2>
            </div>
        );
    }
}

class SiblingCommunicationUsingContext extends Component {
    constructor(props) {
        super(props);
        this.state = { currentCount: 0 };
        this.updateCount = this.updateCount.bind(this);
    }

    updateCount(count) {
        this.setState({ currentCount: count });
    }

    render() {
        return (
            <CounterContext.Provider value={{ count: this.state.currentCount, setCount: this.updateCount }}>
                <Counter />
                <hr />
                <CounterSibling />
            </CounterContext.Provider>
        );
    }
}

export default SiblingCommunicationUsingContext;