// import React, { Component } from 'react';
// import PropTypes from 'prop-types';

// class Counter extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { count: 0, flag: false };
//         this.clickCount = 0;
//         this.inc = this.inc.bind(this);
//         this.dec = this.dec.bind(this);
//         this.reset = this.reset.bind(this);
//     }

//     manageClickCount() {
//         this.clickCount++;
//         if (this.clickCount > 9) {
//             this.setState({ flag: true });
//         }
//     }

//     inc() {
//         this.setState({ count: this.state.count + this.props.interval }, () => { 
//             this.manageClickCount();
//         });
//     }

//     dec() {
//         this.setState({ count: this.state.count - this.props.interval }, () => { 
//             this.manageClickCount();
//         });
//     }

//     reset() {
//         this.clickCount = 0;
//         this.setState({ count: 0, flag: false });
//     }

//     render() {
//         return (
//             <>
//                 <div className="text-center">
//                     <h3 className="text-info">Counter Component</h3>
//                 </div>
//                 <div className="d-grid gap-2 mx-auto col-6">
//                     <input type="text" className="form-control form-control-lg" value={this.state.count} readOnly />
//                     <button className="btn btn-info" disabled={this.state.flag} onClick={this.inc}>
//                         <span className='fs-4'>+</span>
//                     </button>
//                     <button className="btn btn-info" disabled={this.state.flag} onClick={this.dec}>
//                         <span className='fs-4'>-</span>
//                     </button>
//                     <button className="btn btn-secondary" disabled={!this.state.flag} onClick={this.reset}>
//                         <span className='fs-4'>Reset</span>
//                     </button>
//                 </div>
//             </>
//         );
//     }

//     static get defaultProps() {
//         return {
//             interval: 1
//         };
//     }

//     static get propTypes() {
//         return {
//             interval: PropTypes.number
//         };
//     }
// }

// -------------------------------------------------
import React, { Component, useCallback } from 'react';
import PropTypes from 'prop-types';

const Counter = function ({ interval = 1 }) {
    // console.log("Counter Executed...");

    const [count, setCount] = React.useState(0);
    const [flag, setFlag] = React.useState(false);
    const clickCount = React.useRef(0);

    const manageClickCount = useCallback(() => {
        clickCount.current++;
        if (clickCount.current > 9) {
            setFlag(true);
        }
    }, []);

    const inc = useCallback(() => {
        setCount(prevCount => prevCount + interval);
        manageClickCount();
    }, [interval, manageClickCount]);

    const dec = useCallback(() => {
        setCount(prevCount => prevCount - interval);
        manageClickCount();
    }, [interval, manageClickCount]);

    const reset = useCallback(() => {
        setCount(0);
        setFlag(false);
        clickCount.current = 0;
    }, []);

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component - Functional</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={count} readOnly />
                <button className="btn btn-info" disabled={flag} onClick={inc}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info" disabled={flag} onClick={dec}>
                    <span className='fs-4'>-</span>
                </button>
                <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
                    <span className='fs-4'>Reset</span>
                </button>
            </div>
        </>
    );
}

Counter.propTypes = {
    interval: PropTypes.number
};

const CounterAssignment = () => {
    return (
        <div>
            <Counter />
            <hr />
            <Counter interval={10} />
        </div>
    );
};

export default CounterAssignment;

// Recreation of Functions:
// Without useCallback(), the manageClickCount, inc, dec, and reset functions will be recreated on every render.
// This could be inefficient if these functions are passed as props to other components, causing unnecessary re-renders of those components.

// Performance:
// In small applications or components, the difference may be negligible.
// However, in larger applications, avoiding unnecessary re-renders by using useCallback() can improve performance.

// Dependencies Management:
// Using useCallback() helps in maintaining the dependencies correctly and ensuring that the functions are only recreated when necessary.